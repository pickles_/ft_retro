//
// Created by Dmitry Titenko on 11/5/17.
//

#include <IGameController.hpp>
#include <cstdlib>
#include <ncurses.h>
#include <string>
#include <Program.hpp>
#include "GameOver.hpp"

GameOver::GameOver() {

}

GameOver::GameOver(const GameOver &rhs) : IGameController(rhs) {

}

GameOver &GameOver::operator=(const GameOver &) {
    return *this;
}

GameOver::~GameOver() {

}

void GameOver::render() {
    static const char *title[] = {
            "  #####     #    #     # #######    ####### #     # ####### ######  ",
            " #     #   # #   ##   ## #          #     # #     # #       #     # ",
            " #        #   #  # # # # #          #     # #     # #       #     # ",
            " #  #### #     # #  #  # #####      #     # #     # #####   ######  ",
            " #     # ####### #     # #          #     #  #   #  #       #   #   ",
            " #     # #     # #     # #          #     #   # #   #       #    #  ",
            "  #####  #     # #     # #######    #######    #    ####### #     # "
    };
    size_t lines = sizeof(title) / sizeof(title[0]);
    for (size_t i = 0; i < lines; i++)
    {
        std::string title_str = title[i];
        move((LINES - lines) / 2 + i, (COLS - title_str.size()) / 2);
        for (size_t j = 0; j < title_str.size(); j++)
        {
            switch (title_str[j])
            {
                case '#':
                    attrset(COLOR_P(0, color));
                    break;
                default:
                    attrset(COLOR_P(0, COLOR_BLACK));
            }
            addch(' ');
        }
    }
    attrset(COLOR_PAIR(0));
}

void GameOver::eventHandler(int ch)
{
    if (ch == 27)
        exit(0);
}

void GameOver::update()
{
    color = std::rand() % 7 + 1;
}

GameOver &GameOver::getInstance() {
    static GameOver gameOver;
    return gameOver;
}
