//
// Created by dtitenko on 11/4/17.
//

#include <iostream>
#include <ncurses.h>
#include <Program.hpp>
#include <cmath>
#include <cstdlib>
#include <Game.hpp>
#include "Menu.hpp"

Menu::Menu():color(1)
{
}

Menu::~Menu()
{}

Menu::Menu(const Menu &m) : IGameController(m)
{}

Menu& Menu::operator=(const Menu&)
{
	return Menu::getInstance();
}

Menu& Menu::getInstance()
{
	static Menu menu;
	return menu;
}

void Menu::update()
{
	color = std::rand() % 7 + 1;
}

void Menu::render()
{
	static const char *menuItems[] = { "   PLAY   ", "   QUIT   " };
	static const char *title[] = {
			"### ###   #### ### ### #### ####",
			"#    #    #  # #    #  #  # #  #",
			"##   #    ###  ##   #  ###  #  #",
			"#    #    #  # #    #  #  # #  #",
			"#    #    #  # ###  #  #  # ####"
	};

	size_t lines = sizeof(title) / sizeof(title[0]);
	for (size_t i = 0; i < lines; i++)
	{
		std::string title_str = title[i];
		move(i + 4, (COLS - title_str.size()) / 2);
		for (size_t j = 0; j < title_str.size(); j++)
		{
			switch (title_str[j])
			{
				case '#':
					attrset(COLOR_P(0, color));
					break;
				default:
					attrset(COLOR_P(0, COLOR_BLACK));
			}
			addch(' ');
		}
	}
	attrset(COLOR_PAIR(0));

	size_t nOptions = sizeof(menuItems) /sizeof(menuItems[0]);
	for (size_t i = 0; i < nOptions; i++)
	{
		std::string option = menuItems[i];
		move(lines + 7 + i, (COLS - option.size()) / 2);
		size_t(_pos) == i ? attron(A_REVERSE) : attroff(A_REVERSE);
		addstr(menuItems[i]);
	}
}

void Menu::eventHandler(int ch)
{
	switch (ch)
	{
		case 27:
			Program::getInstance().quit();
			break;
		case KEY_UP:
			--_pos %= 2;
			_pos = int(std::abs(_pos));
			break;
		case KEY_DOWN:
			++_pos %= 2;
			break;
		case '\n':
			confirm();
			break;
		default:
			break;
	}
}

int Menu::getPos() const
{
	return _pos;
}

void Menu::confirm()
{
	switch (_pos)
	{
		case 0:
			Program::getInstance().setState(Game::getInstance());
			break;
		case 1:
			Program::getInstance().quit();
			break;
		default:
			break;
	}
}
