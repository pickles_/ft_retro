//
// Created by dtitenko on 11/4/17.
//

#include <ncurses.h>
#include <Program.hpp>
#include <Menu.hpp>
#include <cstdlib>
#include <iostream>
#include <zconf.h>
#include <GameOver.hpp>
#include "Game.hpp"
Game::Game() :
		_over(false),
		score(0),
		lives(4), maxLives(4)
{
	player = new FlyingObject(
			0,
			0, LINES / 2,
			1, true
	);
    player->setPos(0, LINES / 2);
	player->setSpeedX(1);
	/*for (int i = 0; i < MAX_ENEMIES; i++)
		enemies[i] = NULL;*/
}

Game::~Game()
{
	delete player;
	for (int i = 0; i < MAX_ENEMIES; i++)
		if (enemies[i])
			delete enemies[i];
	for (int i = 0; i < MAX_BULLETS; i++)
		if (bullets[i])
			delete bullets[i];
}

Game::Game(const Game &game) : IGameController(game), player(NULL)
{}

Game &Game::operator=(const Game &)
{
	return Game::getInstance();
}

void Game::update()
{
    for (int i = 0; i < MAX_ENEMIES; i++)
        if (enemies[i])
            if (enemies[i]->getX() == player->getX() && enemies[i]->getY() == player->getY())
                gameOver();
	for (int i = 0; i < MAX_ENEMIES; i++)
		if (enemies[i])
		{
			if (enemies[i]->getX() == 0)
				enemies[i]->setState(0);
			enemies[i]->move(-1, 0);
		}
    for (int i = 0; i < MAX_ENEMIES; i++)
        if (enemies[i])
            if (enemies[i]->getX() == player->getX() && enemies[i]->getY() == player->getY())
            {
                delete enemies[i];
                enemies[i] = NULL;
                score++;
                gameOver();
            }
    for (int i = 0; i < MAX_ENEMIES; i++)
    {
        if (!enemies[i])
            continue;
        for (int j = 0; j < MAX_BULLETS; j++)
        {
            if (!bullets[j])
                continue ;
            if (bullets[j]->getState() && bullets[j]->getTeam() != enemies[i]->getTeam() &&
                enemies[i]->getX() == bullets[j]->getX() && enemies[i]->getY() == bullets[j]->getY())
            {
                enemies[i]->setState(0);
                bullets[j]->setState(0);
				score++;
            }
        }
    }
    for (int i = 0; i < MAX_BULLETS; i++)
        if (bullets[i])
        {
            if (bullets[i]->getX() == COLS - 1)
                bullets[i]->setState(0);
            bullets[i]->move(1, 0);
        }
    for (int i = 0; i < MAX_ENEMIES; i++)
    {
        if (!enemies[i])
            continue;
        for (int j = 0; j < MAX_BULLETS; j++)
        {
            if (!bullets[j])
                continue ;
            if (bullets[j]->getState() && bullets[j]->getTeam() != enemies[i]->getTeam() &&
                enemies[i]->getX() == bullets[j]->getX() && enemies[i]->getY() == bullets[j]->getY())
            {
                enemies[i]->setState(0);
                bullets[j]->setState(0);
				score++;
            }
        }
    }
    for (int i = 0; i < MAX_ENEMIES; i++)
        if (enemies[i] && !enemies[i]->getState())
        { delete enemies[i]; enemies[i] = NULL; }
    for (int i = 0; i < MAX_BULLETS; i++)
        if (bullets[i] && !bullets[i]->getState())
        { delete bullets[i]; bullets[i] = NULL; }
	wave();
}

void Game::eventHandler(int ch)
{
	switch (ch)
	{
		case KEY_UP:
			movePlayer(0, -1);
			break;
		case KEY_DOWN:
			movePlayer(0, 1);
			break;
		case KEY_RIGHT:
			movePlayer(1, 0);
			break;
		case KEY_LEFT:
			movePlayer(-1, 0);
			break;
		case 27:
			Program::getInstance().setState(Menu::getInstance());
			break;
		case ' ':
			shoot();
			break;
	}
}

void Game::render()
{
	if (_over)
		return;
	for (int i = 0; i < MAX_ENEMIES; i++)
		if (enemies[i])
			enemies[i]->render(0, 0);
    for (int i = 0; i < MAX_BULLETS; i++)
        if (bullets[i])
            bullets[i]->render(0, 0);
    player->render(0, 0);
	for (int i = 0; i < MAX_ENEMIES; i++)
		if (enemies[i])
			if (enemies[i]->getX() == player->getX() && enemies[i]->getY() == player->getY())
				gameOver();
	attrset(COLOR_P(COLOR_YELLOW, 0) | A_BOLD);
	mvprintw(0, 0, "LIVES:[");
	attrset(COLOR_P(0, COLOR_RED));
	for (int i = 0; i < maxLives; i++) {
		if (i >= lives)
			attrset(COLOR_PAIR(0));
		addch(' ');
	}
	attrset(COLOR_P(COLOR_YELLOW, 0) | A_BOLD);
	addch(']');
    mvprintw(0, COLS - 18, "SCORE: %0.11d", score);
	attrset(COLOR_PAIR(0));
}

Game &Game::getInstance()
{
	static Game game;
	return game;
}

void Game::movePlayer(int x, int y)
{ player->move(x, y); }

void Game::shoot()
{
	FlyingObject *bullet = player->shoot();
    if (!bullet)
        return ;
    bullet->setSpeedX(1);
    for (int i = 0; i < MAX_BULLETS; i++)
        if (bullets[i] == NULL)
        {
            bullets[i] = bullet;
            return ;
        }
    delete bullet;
}

void Game::wave()
{
	static clock_t st = clock();
	clock_t elapsed = (clock() - st);
	if (elapsed > 40000)
		st = clock();
	else
		return;
	int nbEnem = 1 + std::rand() % (LINES / 2);
	while(nbEnem--)
        spawnEnemy();
}

void Game::spawnEnemy()
{
	int y;
	y = std::rand() % (LINES - 1) + 1;
	FlyingObject *enem = new FlyingObject(1, COLS - 1, y, 2, true);
	enem->setSpeedX(std::rand() % 5 + 1);
	for (int i = 0; i < MAX_ENEMIES; i++)
		if (enemies[i] == NULL)
		{
			enemies[i] = enem;
			return ;
		}
	delete enem;
}

void Game::gameOver() {
	if (--lives)
		player->setPos(0, LINES / 2);
	else
	{
		_over = true;
		Program::getInstance().setState(GameOver::getInstance());
	}
}
