//
// Created by dtitenko on 11/5/17.
//

#include <ncurses.h>
#include <Program.hpp>
#include "FlyingObject.hpp"

FObjData FlyingObject::_fobjs[] = {
		FObjData('>', A_BOLD | COLOR_P(COLOR_GREEN, 0)),
		FObjData('0', A_BOLD | COLOR_P(COLOR_RED, 0)),
        FObjData('-', A_BOLD | COLOR_P(COLOR_YELLOW, 0))
};

FlyingObject::FlyingObject() : _index(0), _canShoot(false)
{ }

FlyingObject::~FlyingObject()
{ }

FlyingObject::FlyingObject(const FlyingObject &fobj) : AEntity(fobj)
{ *this = fobj; }

FlyingObject &FlyingObject::operator=(const FlyingObject &rhs)
{
	if (this == &rhs)
		return *this;
	AEntity::operator=(rhs);
	_index = rhs._index;
	_canShoot = rhs._canShoot;
	return *this;
}
FlyingObject::FlyingObject(int index, int x, int y, int team, bool canShoot):
	AEntity(x, y), _index(index), _canShoot(canShoot)
{ _team = team; }

FlyingObject *FlyingObject::shoot()
{
	if (!_canShoot)
        return NULL;
    FlyingObject *bullet = new FlyingObject(2, _x + 1, _y, _team);
    bullet->setSpeedX(2);
    return bullet;
}

void FlyingObject::update()
{ }

int FlyingObject::getChar()
{
	return FlyingObject::_fobjs[_index].getChar();
}
int FlyingObject::getAttr()
{
	return FlyingObject::_fobjs[_index].getAttr();
}
