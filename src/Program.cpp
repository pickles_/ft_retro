//
// Created by dtitenko on 11/4/17.
//

#include <ncurses.h>
#include <iostream>
#include <zconf.h>
#include "Program.hpp"
Program::Program() : _quit(false), _state(NULL)
{
	int mx, my;

	initscr();

	getmaxyx(stdscr, my, mx);

	_width = mx;
	_height = my;


	cbreak();
	noecho();
	nodelay(stdscr, TRUE);
	curs_set(0);
	keypad(stdscr, TRUE);
	ESCDELAY = 5;
	start_color();
	for (short i = 0; i < 100; i++)
		for (short j = 0; j < 100; j++)
			init_pair(COLOR(i, j), i, j);
}

Program::~Program()
{ endwin(); }

Program::Program(const Program &)
{}

Program &Program::operator=(const Program &)
{
	return Program::getInstance();
}

Program &Program::getInstance()
{
	static Program program;
	return program;
}
IGameController *Program::getState() const
{
	return _state;
}
void Program::setState(IGameController &state)
{
	_state = &state;
}
int Program::getWidth() const
{
	return _width;
}
int Program::getHeight() const
{
	return _height;
}

bool Program::isQuit() const
{
	return _quit;
}

void Program::exec()
{
	const int clocksPerFrame = 100000 / 6;
	clock_t after, before;
	int ch;

	if (!_state)
		return ;
	while(!_quit)
	{
		before = clock();
		while((ch = getch()) != ERR)
			_state->eventHandler(ch);
		erase();
		_state->update();
		_state->render();
		refresh();
		after = clock();
		if (after - before < clocksPerFrame)
			usleep(clocksPerFrame - after + before);
	}
}

void Program::quit()
{
	_quit = true;
}
