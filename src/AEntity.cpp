//
// Created by dtitenko on 11/5/17.
//

#include <Program.hpp>
#include <ncurses.h>
#include <ctime>
#include "AEntity.hpp"
AEntity::AEntity(): _x(0), _y(0),
					_speedX(0), _speedY(0),
					_team(0), _state(1), currIter(0)
{}

AEntity::~AEntity()
{}

AEntity::AEntity(const AEntity &entity)
{ *this = entity; }

AEntity &AEntity::operator=(const AEntity &rhs)
{
	if (this == &rhs)
		return *this;
	_x = rhs._x; _y = rhs._y;
	_speedX = rhs._speedX; _speedY = rhs._speedY;
	_state = rhs._state;
	_team = rhs._team;
	return *this;
}


AEntity::AEntity(float x, float y) : _x(x), _y(y),
									 _speedX(0), _speedY(0),
									 _team(0), _state(1), currIter(0)

{}

void AEntity::move(int x, int y)
{
	int nx, ny;

	nx = getX() + x;
	ny = getY() + y;
	currIter++;
	if (!(nx > -1 && nx < COLS && ny > 0 && ny < LINES))
        return;
    if ((currIter < _speedX))
        return;
	currIter %= _speedX;
	setPos(nx, ny);
}

float AEntity::getX() const
{ return _x; }

void AEntity::setX(float x)
{ _x = x; }

float AEntity::getY() const
{ return _y; }

void AEntity::setY(float y)
{ _y = y; }

float AEntity::getSpeedX() const
{ return _speedX; }

void AEntity::setSpeedX(float speedX)
{ _speedX = speedX; }

float AEntity::getSpeedY() const
{ return _speedY; }

void AEntity::setSpeedY(float speedY)
{ _speedY = speedY; }

int AEntity::getTeam() const
{ return _team; }

void AEntity::setTeam(int team)
{ _team = team; }

int AEntity::getState() const
{ return _state; }

void AEntity::setState(int state)
{ _state = state; }

void AEntity::setPos(float x, float y)
{ setX(x); setY(y); }

void AEntity::render(int x, int y)
{
	if (!_state || !(_x > -1 && _x < COLS
		&&  _y > 0 && _y < LINES))
		return;
	move(x, y);
	::move(_y, _x);
	attrset(getAttr());
	addch(getChar());
}
