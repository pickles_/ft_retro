//
// Created by dtitenko on 11/5/17.
//

#include "FObjData.hpp"
FObjData::FObjData()
{ }

FObjData::FObjData(char ch, int attr)
		:_char(ch), _attr(attr)
{ }

FObjData::~FObjData()
{ }

FObjData::FObjData(const FObjData &fObjData)
{ *this = fObjData; }

FObjData &FObjData::operator=(const FObjData &rhs)
{
	if (this == &rhs)
		return *this;
	_char = rhs._char;
	_attr = rhs._attr;
	return *this;
}

int FObjData::getAttr() const
{ return _attr; }
void FObjData::setAttr(int attr)
{ FObjData::_attr = attr; }

char FObjData::getChar() const
{ return _char; }
void FObjData::setChar(char aChar)
{ _char = aChar; }
