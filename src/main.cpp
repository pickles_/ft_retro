//
// Created by dtitenko on 11/4/17.
//

#include <ncurses.h>
#include <iostream>
#include <Program.hpp>
#include <Menu.hpp>
#include <Game.hpp>

int main()
{
	Menu &menu = Menu::getInstance();
	Program &program = Program::getInstance();
	program.setState(menu);
	program.exec();
}