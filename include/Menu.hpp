//
// Created by dtitenko on 11/4/17.
//

#ifndef RUSH00_MENU_H
#define RUSH00_MENU_H

#include "IGameController.hpp"

class Menu: public IGameController
{
	private:
		Menu();
		Menu(const Menu &);
		Menu &operator=(const Menu &);
		~Menu();
		int _pos, color;

	public:
		void render();
		void eventHandler(int);
		void update();
		void confirm();
		static Menu &getInstance();
		int getPos() const;
};

#endif //RUSH00_MENU_H
