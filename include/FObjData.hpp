//
// Created by dtitenko on 11/5/17.
//

#ifndef RUSH00_FOBJDATA_HPP
#define RUSH00_FOBJDATA_HPP

class FObjData
{
	private:
		char _char;
		int _attr;

	public:
		FObjData();
		FObjData(char ch, int attr);
		virtual ~FObjData();
		FObjData(const FObjData &);
		FObjData &operator=(const FObjData &);

		int getAttr() const;
		void setAttr(int attr);
		char getChar() const;
		void setChar(char aChar);

};

#endif //RUSH00_FOBJDATA_HPP
