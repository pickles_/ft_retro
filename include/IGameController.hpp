//
// Created by dtitenko on 11/4/17.
//

#ifndef RUSH00_IGAMECONTROLLER_HPP
#define RUSH00_IGAMECONTROLLER_HPP

class IGameController
{
	public:
		IGameController();
		virtual ~IGameController();

		IGameController(const IGameController &);
		IGameController &operator=(const IGameController &);

		virtual void render() = 0;
		virtual void eventHandler(int ch) = 0;
		virtual void update() = 0;
};

#endif //RUSH00_IGAMECONTROLLER_HPP
