//
// Created by dtitenko on 11/5/17.
//

#ifndef RUSH00_FLYINGOBJECT_HPP
#define RUSH00_FLYINGOBJECT_HPP

#include "AEntity.hpp"
#include "FObjData.hpp"
class FlyingObject : public AEntity
{
	private:
		int _index;
		bool _canShoot;

		static FObjData _fobjs[];

	public:
		FlyingObject();
		~FlyingObject();
		FlyingObject(const FlyingObject &);
		FlyingObject &operator=(const FlyingObject &);
		FlyingObject(int index, int x, int y, int team, bool can_shoot=false);

		FlyingObject *shoot();

		void update();
		int getChar();
		int getAttr();

};

#endif //RUSH00_FLYINGOBJECT_HPP
