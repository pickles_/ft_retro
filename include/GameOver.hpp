//
// Created by Dmitry Titenko on 11/5/17.
//

#ifndef RUSH00_GAMEOVER_HPP
#define RUSH00_GAMEOVER_HPP


class GameOver : public IGameController
{

    private:
        GameOver();
        GameOver(const GameOver &);
        GameOver &operator=(const GameOver &);
        ~GameOver();
        int color;

    public:
        void render();
        void eventHandler(int);
        void update();
        static GameOver&getInstance();
};


#endif //RUSH00_GAMEOVER_HPP
