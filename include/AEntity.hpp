//
// Created by dtitenko on 11/5/17.
//

#ifndef RUSH00_IENTITY_HPP
#define RUSH00_IENTITY_HPP


class AEntity
{
	protected:
		float _x, _y;
		int _speedX, _speedY;
		int _team;
		int _state;

		int currIter;

	public:
		AEntity();
		virtual ~AEntity();
		AEntity(const AEntity &);
		AEntity &operator=(const AEntity &);

		AEntity(float x, float y);

		virtual int getAttr() = 0;
		virtual int getChar() = 0;
		//virtual void ai(Game &game) = 0;
		virtual void update() = 0;
		virtual void render(int x, int y);

		void move(int x, int y);
		void setPos(float x, float y);
		float getX() const;
		void setX(float x);
		float getY() const;
		void setY(float y);
		float getSpeedX() const;
		void setSpeedX(float speedX);
		float getSpeedY() const;
		void setSpeedY(float speedY);
		int getTeam() const;
		void setTeam(int team);
		int getState() const;
		void setState(int state);
};

#endif //RUSH00_IENTITY_HPP
