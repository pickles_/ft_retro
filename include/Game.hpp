//
// Created by dtitenko on 11/4/17.
//

#ifndef RUSH00_GAME_HPP
#define RUSH00_GAME_HPP

#include "IGameController.hpp"
#include "AEntity.hpp"
#include "FlyingObject.hpp"

#define MAX_ENEMIES 100
#define MAX_BULLETS 20

class Game : public IGameController
{
	private:
		Game();
		~Game();

		Game(const Game &);
		Game &operator=(const Game &);

		bool _over;
		FlyingObject *player;
		size_t score;
		int lives;
		int maxLives;

		AEntity *enemies[MAX_ENEMIES];
		AEntity *bullets[MAX_BULLETS];

		void wave();
		void spawnEnemy();
		void gameOver();

	public:
		void update();
		void eventHandler(int);
		void render();
		void movePlayer(int x, int y);
		void shoot();
		static Game &getInstance();

};

#endif //RUSH00_GAME_HPP
