//
// Created by dtitenko on 11/4/17.
//

#ifndef RUSH00_PROGRAM_HPP
#define RUSH00_PROGRAM_HPP

#include "IGameController.hpp"

#define COLOR(fg, bg) (((fg) << 3) + (bg) + 1)
#define COLOR_P(fg, bg) (COLOR_PAIR(COLOR(fg, bg)))

class Program
{
	private:
		Program();
		~Program();

		Program(const Program &);
		Program & operator=(const Program &);

		int _width, _height;
		bool _quit;
		IGameController *_state;
	public:
		static Program &getInstance();
		void exec();
		void quit();

		int getWidth() const;
		int getHeight() const;
		bool isQuit() const;
		IGameController *getState() const;
		void setState(IGameController &state);


};

#endif //RUSH00_PROGRAM_HPP
